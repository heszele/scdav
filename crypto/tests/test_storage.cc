#include <catch2/catch.hpp>
#include <crypto/storage.hh>

#include <string>
#include <sodium.h>

SCENARIO("storage_type::item_type can be used to securely lock sensitive data", "[crypto]")
{
    REQUIRE(sodium_init() != -1);

    GIVEN("An in memory buffer with sensitive data")
    {
        constexpr size_t size = 256;
        scDAV::crypto::byte_type data[size] = { 0 };
        scDAV::crypto::byte_type data_copy[size] = { 0 };

        randombytes_buf(data, size);
        memcpy(data_copy, data, size);

        THEN("A storage_type::item_type object can be easily created from it")
        {
            scDAV::crypto::storage_type::item_type item(data, size);

            CHECK(item.is_locked());
            AND_THEN("The item's size will be correct")
            {
                CHECK(size == item.get_size());
            }
            AND_THEN("The original buffer will be emptied")
            {
                scDAV::crypto::byte_type expected_data[size] = { 0 };
                CHECK(sodium_memcmp(data, expected_data, size) == 0);
            }
            AND_THEN("The original data can be retrieved from the item")
            {
                // Item needs to be unlocked first
                item.unlock();
                CHECK(sodium_memcmp(data_copy, item.get_buffer(), size) == 0);
            }
        }
    }
}

SCENARIO("storage_type::item_type::reference_type can be used to easily work with sensitive data", "[crypto]")
{
    REQUIRE(sodium_init() != -1);

    GIVEN("An in memory buffer with sensitive data")
    {
        constexpr size_t size = 256;
        scDAV::crypto::byte_type data[size] = { 0 };
        scDAV::crypto::byte_type data_copy[size] = { 0 };

        randombytes_buf(data, size);
        memcpy(data_copy, data, size);

        THEN("A storage_type::item_type object can be easily created from it")
        {
            auto* item = new scDAV::crypto::storage_type::item_type(data, size);

            AND_THEN("A storage_type::item_type::reference_type object can be easily created from it")
            {
                auto* reference_a = new scDAV::crypto::storage_type::item_type::reference_type(item);

                CHECK_FALSE(item->is_locked());
                CHECK(*reference_a);
                CHECK((*reference_a)->get_size() == size);
                CHECK(sodium_memcmp(data_copy, (*reference_a)->get_buffer(), size) == 0);

                AND_THEN("When the item deleted, the reference will detect it")
                {
                    delete item;

                    CHECK_FALSE(*reference_a);

                    delete reference_a;
                }
                AND_THEN("Another references can easily be created")
                {
                    auto* reference_b = new scDAV::crypto::storage_type::item_type::reference_type(item);
                    auto* reference_c = new scDAV::crypto::storage_type::item_type::reference_type(*reference_a);

                    CHECK_FALSE(item->is_locked());

                    CHECK(*reference_b);
                    CHECK((*reference_b)->get_size() == size);
                    CHECK(sodium_memcmp(data_copy, (*reference_b)->get_buffer(), size) == 0);

                    CHECK(*reference_c);
                    CHECK((*reference_c)->get_size() == size);
                    CHECK(sodium_memcmp(data_copy, (*reference_c)->get_buffer(), size) == 0);

                    AND_THEN("When all the references destroyed, the item will be locked again")
                    {
                        delete reference_a;
                        CHECK_FALSE(item->is_locked());
                        delete reference_b;
                        CHECK_FALSE(item->is_locked());
                        delete reference_c;
                        CHECK(item->is_locked());

                        delete item;
                    }
                    AND_THEN("Deleting the item makes all the references detect it")
                    {
                        delete item;

                        CHECK_FALSE(*reference_a);
                        CHECK_FALSE(*reference_b);
                        CHECK_FALSE(*reference_c);

                        delete reference_a;
                        delete reference_b;
                        delete reference_c;
                    }
                }
            }
        }
    }
}

SCENARIO("storage_type can by used to safely store sensitive data", "[crypto]")
{
    REQUIRE(sodium_init() != -1);

    GIVEN("An default constructed storage_type object")
    {
        scDAV::crypto::storage_type storage;

        THEN("It is initially empty")
        {
            CHECK(storage.get_size() == 0);
        }
        THEN("storage_type::item_type objects can easily be stored in it")
        {
            constexpr size_t size = 256;
            const std::string name("very_sensitive_data");
            scDAV::crypto::byte_type data[size] = { 0 };
            scDAV::crypto::byte_type data_copy[size] = { 0 };

            randombytes_buf(data, size);
            memcpy(data_copy, data, size);

            storage.store(name, data, size);

            CHECK(storage.get_size() == 1);
            CHECK(storage.has_item(name));

            AND_THEN("The item can be retrieved by its name")
            {
                scDAV::crypto::storage_type::item_type::reference_type item = storage.retrieve(name);

                CHECK(item->get_size() == size);
                CHECK(sodium_memcmp(data_copy, item->get_buffer(), item->get_size()) == 0);

                // The item is still in the storage
                CHECK(storage.get_size() == 1);
                CHECK(storage.has_item(name));
                CHECK_NOTHROW(storage.retrieve(name));
            }
        }
        THEN("Retrieving a non-existent item throws an exception")
        {
            CHECK_THROWS_AS(storage.retrieve("non-existing-item"), scDAV::crypto::storage_type::exception_type);
        }
    }
}