#include <catch2/catch.hpp>
#include <crypto/crypto.hh>
#include <crypto/storage.hh>

#include <sodium.h>

SCENARIO("crypto can encrypt binary streams", "[crypto]")
{
    REQUIRE(sodium_init() != -1);

    GIVEN("An in-memory byte stream")
    {
        constexpr  size_t size = 64;
        auto* buffer = reinterpret_cast<scDAV::crypto::byte_type*>("This is a text to be encrypted soon");
        scDAV::crypto::imstream input(buffer);

        AND_GIVEN("A storage_type object with a stored password in it")
        {
            const std::string password_storage_key("password");
            scDAV::crypto::byte_type password[3] = { 'x', 'y', 'z' };
            scDAV::crypto::storage_type storage;

            storage.store(password_storage_key, password, 3 );

            THEN("The in-memory byte stream ben easily be encrypted")
            {
                scDAV::crypto::omstream output;

                scDAV::crypto::encrypt(input, output, storage);

                AND_THEN("The encoded byte stream can easily be decrypted")
                {
                    scDAV::crypto::imstream encrypted_input(output.str());
                    scDAV::crypto::omstream decrypted_output;

                    scDAV::crypto::decrypt(encrypted_input, decrypted_output, storage);

                    CHECK(decrypted_output.str() == buffer);
                }
                AND_THEN("Using the wrong password will cause the decryption to fail")
                {
                    scDAV::crypto::imstream encrypted_input(output.str());
                    scDAV::crypto::omstream decrypted_output;
                    scDAV::crypto::byte_type wrong_password[3] = { 'a', 'b', 'c' };

                    storage.remove(password_storage_key);
                    storage.store(password_storage_key, wrong_password, 3);
                    CHECK_THROWS_AS(scDAV::crypto::decrypt(encrypted_input, decrypted_output, storage),
                                    scDAV::crypto::exception_type);
                }
                AND_THEN("Changing the decrypted message will cause the decryption to fail")
                {
                    auto encrypted_message = output.str();
                    const size_t index = encrypted_message.size() / 2;
                    const scDAV::crypto::byte_type encrypted_byte = encrypted_message[index];

                    encrypted_message[index] = encrypted_byte + 1;

                    scDAV::crypto::imstream encrypted_input(encrypted_message);
                    scDAV::crypto::omstream decrypted_output;

                    CHECK_THROWS_AS(scDAV::crypto::decrypt(encrypted_input, decrypted_output, storage),
                                    scDAV::crypto::exception_type);
                }
            }
        }
    }
}
