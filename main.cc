#include <Poco/Util/ServerApplication.h>
#include <Poco/MemoryStream.h>

#include <crypto/crypto.hh>

#include <string>
#include <sstream>

class ScDavServer: public Poco::Util::ServerApplication
{

};


int main(int argc, char** argv)
{
    ScDavServer app;

    return app.run(argc, argv);
}
