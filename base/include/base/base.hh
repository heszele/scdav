#pragma once
#include <exception>
#include <string>

namespace scDAV
{
    class exception_type: public std::exception
    {
    public:
        exception_type(std::string message);

        virtual const char* what() const noexcept override;

    private:
        std::string _message;
    };
}
