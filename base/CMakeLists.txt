add_library(scdav_base STATIC src/base.cc include/base/base.hh)
target_include_directories(scdav_base PUBLIC include)
set_target_properties(scdav_base PROPERTIES FOLDER ${SCDAV_LIB_FOLDER})

add_library(scDAV::base ALIAS scdav_base)
