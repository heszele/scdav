#include <crypto/crypto.hh>
#include <crypto/storage.hh>
#include <sodium.h>

namespace scDAV::crypto
{
    void encrypt(istream& input, ostream& output, storage_type& storage)
    {
        if(!input)
        {
            throw exception_type("Invalid input stream!");
        }
        if(!output)
        {
            throw exception_type("Invalid output stream!");
        }

        //
        // Generate seed from password and salt
        //

        storage_type::item_type::reference_type password = storage.retrieve("password");
        byte_type seed[crypto_box_SEEDBYTES] = { 0 };
        byte_type salt[crypto_pwhash_SALTBYTES] = { 0 };

        randombytes_buf(salt, sizeof(salt));

        if (crypto_pwhash(seed, sizeof(seed),
                        reinterpret_cast<char*>(password->get_buffer()), password->get_size(),
                        salt,
                        crypto_pwhash_OPSLIMIT_INTERACTIVE,
                        crypto_pwhash_MEMLIMIT_INTERACTIVE,
                        crypto_pwhash_ALG_DEFAULT) != 0)
        {
            throw exception_type("Failed to hash password!");
        }

        output.write(salt, sizeof(salt));

        //
        // Generate key pair
        //

        byte_type public_key[crypto_box_PUBLICKEYBYTES];
        byte_type secret_key[crypto_box_SECRETKEYBYTES];

        if(crypto_box_seed_keypair(public_key, secret_key, seed) != 0)
        {
            throw exception_type("Failed to generate key pair!");
        }

        //
        // Generate key for file encryption
        //

        byte_type file_key[crypto_secretstream_xchacha20poly1305_KEYBYTES];
        crypto_secretstream_xchacha20poly1305_keygen(file_key);

        //
        // Encrypt file key with public key
        //

        byte_type encrypted_file_key[crypto_box_MACBYTES + crypto_secretstream_xchacha20poly1305_KEYBYTES] = { 0 };
        byte_type nonce[crypto_box_NONCEBYTES];

        randombytes_buf(nonce, sizeof(nonce));

        if(crypto_box_easy(encrypted_file_key, file_key, crypto_secretstream_xchacha20poly1305_KEYBYTES, nonce, public_key, secret_key) != 0)
        {
            throw exception_type("Failed to encrypt file key!");
        }

        //
        // Write encoded file key to file
        //

        output.write(nonce, crypto_box_NONCEBYTES);
        output.write(encrypted_file_key, crypto_box_MACBYTES + crypto_secretstream_xchacha20poly1305_KEYBYTES);

        //
        // Encode input file with file key
        //

        crypto_secretstream_xchacha20poly1305_state state;
        byte_type header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];

        if(crypto_secretstream_xchacha20poly1305_init_push(&state, header, file_key) != 0)
        {
            throw exception_type("Failed to initialize encryption stream!");
        }

        output.write(header, crypto_secretstream_xchacha20poly1305_HEADERBYTES);

        while(!input.eof())
        {
            byte_type buffer[256] = { 0 };
            byte_type encrypted_buffer[256 + crypto_secretstream_xchacha20poly1305_ABYTES] = { 0 };
            unsigned long long encrypted_buffer_length = 0;

            input.read(buffer, 256);
            byte_type tag = input.eof() ? crypto_secretstream_xchacha20poly1305_TAG_FINAL : 0;
            if(crypto_secretstream_xchacha20poly1305_push(&state,
                                                        encrypted_buffer, &encrypted_buffer_length,
                                                        buffer, input.gcount(),
                                                        nullptr, 0,
                                                        tag) != 0)
            {
                throw exception_type("Failed to encrypt data!");
            }

            output.write(encrypted_buffer, encrypted_buffer_length);
        }
    }

    void decrypt(istream& input, ostream& output, storage_type& storage)
    {
        if(!input)
        {
            throw exception_type("Invalid input stream!");
        }
        if(!output)
        {
            throw exception_type("Invalid output stream!");
        }

        storage_type::item_type::reference_type password = storage.retrieve("password");

        //
        // Read salt from the file - it is unencrypted
        //

        unsigned char salt[crypto_pwhash_SALTBYTES] = { 0 };

        input.read(salt, crypto_pwhash_SALTBYTES);

        //
        // Generate seed from password and salt
        //

        unsigned char seed[crypto_box_SEEDBYTES] = { 0 };

        if (crypto_pwhash(seed, sizeof(seed),
                          reinterpret_cast<char*>(password->get_buffer()), password->get_size(),
                          salt,
                          crypto_pwhash_OPSLIMIT_INTERACTIVE,
                          crypto_pwhash_MEMLIMIT_INTERACTIVE,
                          crypto_pwhash_ALG_DEFAULT) != 0)
        {
            throw exception_type("Failed to hash password");
        }

        //
        // Generate key pair from seed
        //

        unsigned char public_key[crypto_box_PUBLICKEYBYTES];
        unsigned char secret_key[crypto_box_SECRETKEYBYTES];

        if(crypto_box_seed_keypair(public_key, secret_key, seed) != 0)
        {
            throw exception_type("Failed to generate key pair!");
        }

        //
        // Read nonce and file key - file key is encrypted
        //

        unsigned char encrypted_file_key[crypto_box_MACBYTES + crypto_secretstream_xchacha20poly1305_KEYBYTES] = { 0 };
        unsigned char nonce[crypto_box_NONCEBYTES];

        input.read(nonce, crypto_box_NONCEBYTES);
        input.read(encrypted_file_key, crypto_box_MACBYTES + crypto_secretstream_xchacha20poly1305_KEYBYTES);

        //
        // Decrypt file key
        //

        unsigned char file_key[crypto_secretstream_xchacha20poly1305_KEYBYTES] = { 0 };

        if(crypto_box_open_easy(file_key, encrypted_file_key, crypto_box_MACBYTES + crypto_secretstream_xchacha20poly1305_KEYBYTES, nonce, public_key, secret_key) != 0)
        {
            throw exception_type("Failed to decrypt file key!");
        }

        //
        // Decrypt file with file key
        //

        crypto_secretstream_xchacha20poly1305_state state;
        unsigned char header[crypto_secretstream_xchacha20poly1305_HEADERBYTES];

        input.read(header, crypto_secretstream_xchacha20poly1305_HEADERBYTES);

        if(crypto_secretstream_xchacha20poly1305_init_pull(&state, header, file_key) != 0)
        {
            throw exception_type("Failed to initialize decryption stream!");
        }

        while(!input.eof())
        {
            unsigned char buffer[256] = { 0 };
            unsigned long long buffer_length = 0;
            unsigned char encrypted_buffer[256 + crypto_secretstream_xchacha20poly1305_ABYTES] = { 0 };
            unsigned char tag = 0;

            input.read(encrypted_buffer, 256 + crypto_secretstream_xchacha20poly1305_ABYTES);

            if(crypto_secretstream_xchacha20poly1305_pull(&state,
                                                          buffer, &buffer_length,
                                                          &tag,
                                                          encrypted_buffer, input.gcount(),
                                                          nullptr, 0) != 0)
            {
                throw exception_type("Failed to decrypt data!");
            }
            output.write(buffer, buffer_length);
        }
    }
}
