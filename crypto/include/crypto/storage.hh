#pragma once
#include "crypto.hh"

#include <map>
#include <string>

namespace scDAV::crypto
{
    class storage_type
    {
    public:
        class exception_type: public scDAV::crypto::exception_type
        {
        public:
            using scDAV::crypto::exception_type::exception_type;
        };

        class item_type
        {
            struct control_block_type;

        public:
            class reference_type
            {
            public:
                reference_type(item_type* item);
                reference_type(const reference_type& other);
                reference_type(reference_type&& other) noexcept;

                ~reference_type();

                reference_type& operator=(const reference_type& other);
                reference_type& operator=(reference_type&& other) noexcept;

                item_type* operator->();
                const item_type* operator->() const;

                item_type& operator*();
                const item_type& operator*() const;

                explicit operator bool() const;

            private:
                void release();

                item_type* _item = nullptr;
                control_block_type* _control_block = nullptr;
            };

            item_type(byte_type* buffer, size_t size);
            item_type(const item_type&) = delete;
            item_type(item_type&& other) noexcept = delete;

            ~item_type();

            void lock();
            void unlock();
            bool is_locked() const;

            byte_type* get_buffer();
            [[nodiscard]] const byte_type* get_buffer() const;

            [[nodiscard]] size_t get_size() const;

            item_type& operator=(const item_type&) = delete;
            item_type& operator=(item_type&& other) noexcept = delete;

        private:
            struct control_block_type
            {
                uint32_t _reference_counter = 0;
                bool _deleted = false;
            };

            void release();

            byte_type* _buffer = nullptr;
            size_t _size = 0;
            control_block_type* _control_block = nullptr;
            bool _locked = false;
        };

        storage_type() = default;

        void store(std::string name, byte_type* buffer, size_t size);
        item_type::reference_type retrieve(const std::string& name);
        void remove(const std::string& name);

        [[nodiscard]] size_t get_size() const;
        [[nodiscard]] bool has_item(const std::string& name) const;

    private:
        std::map<std::string, item_type> _storage;
    };
}
