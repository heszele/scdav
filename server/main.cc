#include <Poco/Util/ServerApplication.h>

#include <string>
#include <vector>

namespace Poco::Util
{
    class OptionSet;
}

namespace scDAV
{
    class server_type: public Poco::Util::ServerApplication
    {
    private:
        virtual void initialize(Poco::Util::Application& self) override
        {
            
        }
        virtual void uninitialize() override
        {
            
        }
        virtual void reinitialize(Poco::Util::Application& self) override
        {
            
        }

        virtual void defineOptions(Poco::Util::OptionSet& options) override
        {
            
        }
        virtual void handleOption(const std::string& name, const std::string& value) override
        {
            
        }

        virtual int main(const std::vector<std::string>& args) override
        {
            waitForTerminationRequest();

            return Poco::Util::Application::EXIT_OK;
        }
    };
}

// TODO: It does not work. Calls run() which is a protected method
//POCO_APP_MAIN(scDAV::server_type)

int main(int argc, char** argv)
{
    scDAV::server_type server;

    return server.run(argc, argv);
}
