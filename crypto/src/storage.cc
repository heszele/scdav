#include <crypto/storage.hh>

#include <sodium.h>

#include <cstring>
#include <sstream>

namespace scDAV::crypto
{
    //
    // storage_type::item_type::reference_type
    //

    storage_type::item_type::reference_type::reference_type(item_type* item):
        _item(item)
    {
        if(nullptr != _item)
        {
            _control_block = _item->_control_block;
            if(nullptr == _control_block)
            {
                _control_block = new control_block_type();
                item->_control_block = _control_block;
                item->unlock();
            }
            ++_control_block->_reference_counter;
        }
    }

    storage_type::item_type::reference_type::reference_type(const reference_type& other):
        _item(other._item),
        _control_block(other._control_block)
    {
        if(nullptr != _control_block)
        {
            ++_control_block->_reference_counter;
        }
    }

    storage_type::item_type::reference_type::reference_type(reference_type&& other) noexcept:
        _item(other._item),
        _control_block(other._control_block)
    {
        other._item = nullptr;
        other._control_block = nullptr;
    }

    storage_type::item_type::reference_type::~reference_type()
    {
        release();
    }

    storage_type::item_type::reference_type& storage_type::item_type::reference_type::operator=(const reference_type& other)
    {
        if(this == &other)
        {
            return *this;
        }

        release();

        _control_block = other._control_block;
        _item = other._item;
        if(nullptr != _control_block)
        {
            ++_control_block->_reference_counter;
        }

        return *this;
    }

    storage_type::item_type::reference_type& storage_type::item_type::reference_type::operator=(reference_type&& other) noexcept
    {
        if(this == &other)
        {
            return *this;
        }

        release();

        _control_block = other._control_block;
        _item = other._item;
        other._control_block = nullptr;
        other._item = nullptr;

        return *this;
    }

    storage_type::item_type* storage_type::item_type::reference_type::operator->()
    {
        if(nullptr != _control_block && !_control_block->_deleted)
        {
            return _item;
        }

        return nullptr;
    }

    const storage_type::item_type* storage_type::item_type::reference_type::operator->() const
    {
        if(nullptr != _control_block && !_control_block->_deleted)
        {
            return _item;
        }

        return nullptr;
    }

    storage_type::item_type& storage_type::item_type::reference_type::operator*()
    {
        return *operator->();
    }

    const storage_type::item_type& storage_type::item_type::reference_type::operator*() const
    {
        return *operator->();
    }

    storage_type::item_type::reference_type::operator bool() const
    {
        return operator->() != nullptr;
    }

    void storage_type::item_type::reference_type::release()
    {
        if(nullptr != _control_block)
        {
            if(0 == _control_block->_reference_counter)
            {
                // TODO: Error handling. We should not throw exception here
                // throw storage_type::exception_type("Internal error! Reference counter is already zero!");
            }

            --_control_block->_reference_counter;

            if(0 == _control_block->_reference_counter)
            {
                if(_control_block->_deleted)
                {
                    _item = nullptr;
                }

                delete _control_block;
                _control_block = nullptr;

                if(nullptr != _item)
                {
                    _item->_control_block = nullptr;
                    _item->lock();
                }
            }
        }
    }

    //
    // storage_type::item_type
    //

    storage_type::item_type::item_type(byte_type* buffer, size_t size):
        _buffer(static_cast<byte_type*>(sodium_malloc(size))),
        _size(size)
    {
        if(nullptr == _buffer)
        {
            throw storage_type::exception_type("Failed to create storage item!");
        }
        memcpy(_buffer, buffer, size);
        sodium_memzero(buffer, size);
        lock();
    }

    storage_type::item_type::~item_type()
    {
        release();
    }

    void storage_type::item_type::lock()
    {
        if(-1 == sodium_mprotect_noaccess(_buffer))
        {
            throw  storage_type::exception_type("Failed to lock item!");
        }
        _locked = true;
    }

    void storage_type::item_type::unlock()
    {
        if(-1 == sodium_mprotect_readwrite(_buffer))
        {
            throw storage_type::exception_type("Failed to unlock item!");
        }
        _locked = false;
    }

    bool storage_type::item_type::is_locked() const
    {
        return _locked;
    }

    byte_type* storage_type::item_type::get_buffer()
    {
        return _buffer;
    }

    const byte_type* storage_type::item_type::get_buffer() const
    {
        return _buffer;
    }

    size_t storage_type::item_type::get_size() const
    {
        return _size;
    }

    void storage_type::item_type::release()
    {
        sodium_free(_buffer);
        _buffer = nullptr;
        _size = 0;
        if(nullptr != _control_block)
        {
            _control_block->_deleted = true;
        }
    }

    //
    // storage_type
    //

    void storage_type::store(std::string name, byte_type* buffer, size_t size)
    {
        if(_storage.find(name) != _storage.end())
        {
            std::stringstream stream;

            stream << "Item with name " << name << " is already in the storage!";

            throw exception_type(stream.str());
        }
        // Everything has to be emplaced since item_type is not even movable
        _storage.emplace(std::piecewise_construct,
                         std::forward_as_tuple(std::move(name)),
                         std::forward_as_tuple(buffer, size));
    }

    storage_type::item_type::reference_type storage_type::retrieve(const std::string& name)
    {
        auto position = _storage.find(name);

        if(_storage.find(name) == _storage.end())
        {
            std::stringstream stream;

            stream << "No such item with name " << name << "!";

            throw exception_type(stream.str());
        }

        item_type& item = position->second;

        return { &item };
    }

    void storage_type::remove(const std::string& name)
    {
        const auto position = _storage.find(name);

        if(position == _storage.end())
        {
            std::stringstream stream;

            stream << "No such item with name " << name << "!";

            throw exception_type(stream.str());
        }
        _storage.erase(position);
    }

    size_t storage_type::get_size() const
    {
        return _storage.size();
    }

    bool storage_type::has_item(const std::string& name) const
    {
        return _storage.find(name) != _storage.end();
    }
}
