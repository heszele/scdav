#pragma once
#include <base/base.hh>

#include <fstream>
#include <sstream>

namespace scDAV::crypto
{
    using byte_type = unsigned char;
    using istream = std::basic_istream<byte_type, std::char_traits<byte_type>>;
    using ostream = std::basic_ostream<byte_type, std::char_traits<byte_type>>;
    using ifstream = std::basic_ifstream<byte_type, std::char_traits<byte_type>>;
    using ofstream = std::basic_ofstream<byte_type, std::char_traits<byte_type>>;
    using imstream = std::basic_istringstream<byte_type, std::char_traits<byte_type>>;
    using omstream = std::basic_ostringstream<byte_type, std::char_traits<byte_type>>;

    class storage_type;

    class exception_type: public scDAV::exception_type
    {
    public:
        using scDAV::exception_type::exception_type;
    };

    void encrypt(istream& input, ostream& output, storage_type& storage);
    void decrypt(istream& input, ostream& output, storage_type& storage);
}